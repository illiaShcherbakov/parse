const db = require('mongodb').MongoClient;
const toSkip = require('./../utils/toSkip');
const options = require('../db/opt');

function getLatestRates (req, res) {
  db.connect('mongodb://admin:admin@ds117109.mlab.com:17109/parce', options, (err, db) => {
    if (err) throw err;
    var d = [];
    db.listCollections().toArray((err, items) => {
      for(var i = 0; i< items.length; i++) {
        var name = items[i].name;
        if ( name != 'system.indexes' && name != 'accounts' ) {
          var count = 0;
          db.collection(name).find().toArray((err, result) => {
            d.push(result[result.length-1]);
            if (count == items.length-toSkip) {
              res.send(d);
              db.close();
            }
            count++;
          });
        }
      }
    });
  });
}

module.exports = getLatestRates;