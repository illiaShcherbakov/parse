const needle = require('needle');
const cheerio = require('cheerio');

function getDataMinfin( site, callback ) {
  var c = 0;
  var returnData = {
    usd: [],
    eur:[]
  };
  for ( let curr in site ) {
    needle.get( site[curr], (error, response) => {
        var $ = cheerio.load(response.body);
        var data1 = $('.mfm-posr').text().split(" ");
        var data = $('.mfm-posr').text().split(" ").filter( (elem) => {
          return elem.length > 0 && elem != '+' && elem != '-';
        });

        data.forEach( function(element, index, array) {
          if ( index % 2 == 0 ) {
            if ( index < 3 ) {
              returnData[curr].push(element);
            }
          }
        });

        if (c == 1) {
          callback(returnData);
        }
      c++;
    });
  }
}

module.exports = getDataMinfin;