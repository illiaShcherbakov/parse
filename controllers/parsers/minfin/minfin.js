const needle = require('needle');
const cheerio = require('cheerio');
const MinfinRecord = require('../../../models/minfin');
const isDateInDB = require('../../../utils/isDateInDB');
const getDataMinfin = require('./getDataMinfin');


function parseMinfin ( db, site, callback ) {

  var collection = db.collection('minfin.com.ua');

  //Получаем все курсы по валютам
  var gotData = getDataMinfin(site, (gotData) => {

    collection.find().toArray( (err, result) => {

      //Cмотрим текущую дату
      var d = new Date();
      var dStr = d.getUTCDate() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCFullYear();

      //Создаем новый документ в коллекции с курсом и датой
      if ( isDateInDB(result, dStr) == false ) {

        var record = new MinfinRecord({
          date: dStr,
          name: 'minfin.com.ua',
          usd: {
            buy: gotData.usd[0],
            sell: gotData.usd[1]
          },
          eur: {
            buy: gotData.eur[0],
            sell: gotData.eur[1]
          }
        });

        record.save((err) => {
          if (err) throw err;

          console.log('Minfin saved successfully!');
          callback();
        })
      } else {
        callback();
      }
    });
  });
}

module.exports = parseMinfin;
