"use strict";

const needle = require('needle');
const cheerio = require('cheerio');
const FinanceRecord = require('../../models/finance');
var isDateInDB = require('../../utils/isDateInDB');

function parseFinance( db, site, callback) {
  needle.get(site["all"], (error, response) => {
    if (!error && response.statusCode == 200) {
      const $ = cheerio.load(response.body);
      const data = $('.c2, .c3').text().split(' ').splice(6, 4);

      const collection = db.collection('finance.ua');
      collection.find().toArray( (err, result) => {

        //Cмотрим текущую дату
        var d = new Date();
        var dStr = d.getUTCDate() + "-" + (d.getUTCMonth()+1) + "-" + d.getUTCFullYear();

        //Создаем новый документ в коллекции с курсом и датой
        if ( isDateInDB(result, dStr) == false ) {
          var record = new FinanceRecord({
            date: dStr,
            name: 'finance.ua',
            usd: {
              buy: data[0],
              sell: data[1]
            },
            eur: {
              buy: data[2],
              sell: data[3]
            }
          });

          record.save((err) => {
            if (err) throw err;

            console.log('Finance saved successfully!');
            callback();
          })
        } else {
          callback();
        }
      })
    }
  })
}

module.exports = parseFinance;
