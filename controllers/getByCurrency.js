const db = require('mongodb').MongoClient;
const toSkip = require('./../utils/toSkip');
const options = require('../db/opt');

function getByCurrency (req, res) {
  console.log(req.params);
  var currency = req.params.currency;
  db.connect('mongodb://admin:admin@ds117109.mlab.com:17109/parce', options, (err, db) => {
    if (err) throw err;
    var d = [];
    db.listCollections().toArray((err, items) => {
      for(var i = 0; i< items.length-1; i++) {
        var name = items[i].name;
        if ( name !== 'system.indexes' && name != 'accounts' ) {
          var count = 0;
          db.collection(name).find().toArray((err, result) => {
            result[result.length-1][currency]["name"] = result[result.length-1].name;
            d.push(result[result.length-1][currency]);
            if (count == items.length-toSkip) {
              res.send(d);
              db.close();
            }
            count++;
          });
        }
      }
    });
  });
}

module.exports = getByCurrency;