const db = require('mongodb').MongoClient;
const toSkip = require('./../utils/toSkip');
const options = require('../db/opt');

function getByDate(req, res) {
  var {d,m,y} = req.params;
  var userDate = d +"-"+ m +"-"+ y;

  db.connect('mongodb://admin:admin@ds117109.mlab.com:17109/parce', options, (err, db) => {
    if (err) throw err;
    var d = [];
    db.listCollections().toArray((err, items) => {
      for(var i = 0; i< items.length-1; i++) {
        var name = items[i].name;
        if ( name !== 'system.indexes' ) {
          var count = 0;
          db.collection(name).find().toArray((err, result) => {
            var s = result.forEach((elem, index, arr)=>{
              if ( elem["date"] == userDate ) {
                d.push(elem);
              }
            });

            if (count == items.length-toSkip) {
              if ( d.length == 0) {
                res.status(204).send("No data for this date. Please, check the date or choose another one.");
              } else {
                res.send(d);
                db.close();
              }
            }
            count++;
          });
        }
      }
    });
  });
}

module.exports = getByDate;