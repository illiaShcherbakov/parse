"use strict";

const sites = require('../../sites');
const update = require('./updateController');

module.exports = function(req, res) {
  update(sites, () => {
    res.send('Database updated!');
  });
};