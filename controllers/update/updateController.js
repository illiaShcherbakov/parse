//const sites = require("../sites");
const parseMinfin = require('./../parsers/minfin/minfin');
const parseFinance = require('./../parsers/finance');
//const sites = require('../sites');
var db = require('mongodb').MongoClient;

function update(sites, callback) {
  var options = {
    server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } }
  };

  db.connect('mongodb://admin:admin@ds117109.mlab.com:17109/parce', options, (err, db) => {
    if (err) throw err;

    var closeDB = false;

    parseMinfin( db, sites["minfin.com.ua"], () => {
      if (closeDB) {
        db.close(()=>{
          console.log("Minfin Updated");
          callback();
        });
      } else {
        console.log("Minfin Updated");
        closeDB = true;
      }
    });

    parseFinance( db, sites['finance.ua'], () => {
      if (closeDB) {
        db.close(()=>{
          console.log("Finance Updated");
          callback();
        });
      } else {
        console.log("Finance Updated");
        closeDB = true;
      }
    })

  });
}

module.exports = update;