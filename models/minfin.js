var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var MinfinRecord = new Schema({
  date: String,
  name: String,
  usd: {
    buy: Number,
    sell: Number
  },
  eur: {
    buy: Number,
    sell: Number
  }
});

module.exports = mongoose.model('Minfin', MinfinRecord, 'minfin.com.ua');