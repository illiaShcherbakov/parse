var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FinanceRecord = new Schema({
  date: String,
  name: String,
  usd: {
    buy: Number,
    sell: Number
  },
  eur: {
    buy: Number,
    sell: Number
  }
});

module.exports = mongoose.model('Finance', FinanceRecord, 'finance.ua');