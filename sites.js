const sites = {
  "minfin.com.ua": {
    "usd": "http://minfin.com.ua/currency/usd/",
    "eur": "http://minfin.com.ua/currency/eur/"
  },
  "finance.ua": {
    "all": "http://finance.ua/ru/currency"
  },
  "goverla.ua": {
    "all": "https://goverla.ua/"
  }
};

module.exports = sites;