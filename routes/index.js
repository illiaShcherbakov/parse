var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();
const update = require('../controllers/update/index');
const getByDate = require('../controllers/getByDate');
const getByCurrency = require('../controllers/getByCurrency');
const getLatestRates = require('../controllers/getLatestRates');
var loggedIn = require('../utils/loggedIn');

router.get('/', function (req, res) {
  res.render('index', { user : req.user });
});

router.get('/register', function(req, res) {
  res.render('register', { });
});

router.post('/register', function(req, res) {
  Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
    if (err) {
      return res.render('register', { account : account });
    }

    passport.authenticate('local')(req, res, function () {
      res.redirect('/');
    });
  });
});

router.get('/login', function(req, res) {
  res.render('login', { user : req.user });
});

router.post('/login', passport.authenticate('local'), function(req, res) {
  res.redirect('/');
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.get('/ping', function(req, res){
  res.status(200).send("pong!");
});

router.get('/rates', getLatestRates);
router.get('/currency/:currency', getByCurrency);
router.get('/date/:d-:m-:y', getByDate);
router.get('/update', update);

module.exports = router;